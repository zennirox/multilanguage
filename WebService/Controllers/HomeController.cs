﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DatabaseService;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;
using WebService.Models;

namespace WebService.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDatabaseReader<TranslatedPizza> _databaseReader;
        private readonly IUserDeliver _userDeliver;

        public HomeController(
            IDatabaseReader<TranslatedPizza> databaseReader,
            IContextDeliverer<ILanguageElement> contextDeliverer
        )
        {
            _databaseReader = databaseReader;
            if (contextDeliverer is IUserDeliver userDeliver)
            {
                _userDeliver = userDeliver;
            }
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult PizzaMenu(string languageCode)
        {
            _databaseReader.ConnectDatabase("localhost:27017");
            if (string.IsNullOrEmpty(languageCode))
            {
                languageCode = Language.English;
            }
            _userDeliver?.SetSupportedLanguage(languageCode);

            ICollection<TranslatedPizza> resultFromDb;

            try
            {
                resultFromDb = _databaseReader.GetData("local", "pizza");
                if (resultFromDb == null)
                {
                    //TODO: msg error db returned null
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                return BadRequest();
                //TODO: add logger
            }
           
            var pizzaViewModels = resultFromDb.Select(p =>
                new PizzaViewModel(
                    p.Price,
                    p.ImageUrl,
                    p.PizzaContent.Name,
                    p.PizzaContent.Description,
                    p.PizzaContent.LanguageCode
                )
            ).ToArray();
            return View(pizzaViewModels);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel
            {
                RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier
            });
        }
    }
}
