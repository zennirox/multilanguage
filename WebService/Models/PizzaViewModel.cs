﻿namespace WebService.Models
{
    public class PizzaViewModel
    {
        public PizzaViewModel(
            double price,
            string imageUrl,
            string name,
            string description,
            string languageCode
        )
        {
            Price = price;
            ImageUrl = imageUrl;
            Name = name;
            Description = description;
            LanguageCode = languageCode;
        }
        public double Price { get; }
        public string ImageUrl { get; }
        public string Name { get; }
        public string Description { get; }
        public string LanguageCode { get; }
    }
}
