﻿namespace DatabaseService
{
    public class TranslatedPizza
    {
        public TranslatedPizza(double price, string imageUrl, PizzaContent content)
        {
            Price = price;
            ImageUrl = imageUrl;
            PizzaContent = content;
        }
        public double Price { get; }
        public string ImageUrl { get; }
        public PizzaContent PizzaContent { get; }
    }
}
