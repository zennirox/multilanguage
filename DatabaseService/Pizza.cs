﻿using System.Collections.Generic;
using Infrastructure;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DatabaseService
{
    public class Pizza
    {
        [BsonId]
        [BsonElement("_id")]
        public ObjectId Id { get; set; }
        public double Price { get; set; }
        public string ImageUrl { get; set; }
        public ICollection<PizzaContent> Contents { get; set; }
    }

    public class PizzaContent : ILanguageElement
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string LanguageCode { get; set; }
    }
}
