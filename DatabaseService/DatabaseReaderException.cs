﻿using System;
using System.Runtime.Serialization;

namespace DatabaseService
{
    public class DatabaseReaderException : Exception
    {
        public DatabaseReaderException()
        {
        }

        public DatabaseReaderException(string message) : base(message)
        {
        }

        public DatabaseReaderException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected DatabaseReaderException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
