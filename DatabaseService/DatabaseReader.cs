﻿using System;
using System.Collections.Generic;
using System.Linq;
using Infrastructure;
using MongoDB.Driver;

namespace DatabaseService
{
    public class DatabaseReader : IDatabaseReader<TranslatedPizza>
    {
        private readonly IContextDeliverer<ILanguageElement> _contextDeliverer;
        private MongoClient _mongoClient;

        public DatabaseReader(IContextDeliverer<ILanguageElement> contextDeliverer)
        {
            _contextDeliverer = contextDeliverer;
        }

        /// <summary>
        /// Connect to passed db address
        /// </summary>
        /// <param name="dbAddress"></param>
        public void ConnectDatabase(string dbAddress)
        {
            //TODO: add regex for db address
            if (string.IsNullOrEmpty(dbAddress))
            {
                throw new ArgumentException(nameof(dbAddress));
            }

            if (dbAddress.Contains("mongodb"))
            {
                throw new InvalidOperationException("Pass without mongodb key word");
            }
            _mongoClient = new MongoClient($"mongodb://{dbAddress}");

        }

        /// <summary>
        /// Read data from database
        /// </summary>
        /// <exception cref="DatabaseReaderException">Cannot get database</exception>
        /// <exception cref="DatabaseReaderException">There is no column : pizza in database</exception>
        /// <returns></returns>
        public ICollection<TranslatedPizza> GetData(string dbName, string columnName)
        {
            var db = _mongoClient.GetDatabase(dbName);
            if (db == null)
            {
                throw new DatabaseReaderException(
                    $"Cannot get database: {dbName}"
                );
            }
            var col = db.GetCollection<Pizza>(columnName)
                .AsQueryable()
                ?.ToArray();
            if (col == null)
            {
                throw new DatabaseReaderException(
                    $"There is no column: {columnName} in database"
                );
            }
            return col.Select(p => new TranslatedPizza(
                p.Price,
                p.ImageUrl,
                (PizzaContent)_contextDeliverer.DeliverData(p.Contents.Cast<ILanguageElement>().ToList()))
            ).ToArray();
        }
    }
   
}
