﻿using Autofac;
using ContextDeliver;
using DatabaseService;
using Infrastructure;

namespace MutliLang
{
    public class ContainerConfigurator
    {
        public IContextDeliverer<ILanguageElement> BuildDelivers(string userDefLang)
        {
            var userDeliver = new UserDeliver(userDefLang);
            var engDeliver = new EnglishDeliver(Language.English);
            var polDeliver = new PolishDeliver(Language.Polish);
            var anyDeliver = new AnyDeliver(Language.Any);

            userDeliver.SetNext(engDeliver).SetNext(polDeliver).SetNext(anyDeliver);
            return userDeliver;
        }
        public IContainer Configure(
            ContainerBuilder builder,
            IContextDeliverer<ILanguageElement> userDeliver
        )
        {
           
            builder.RegisterInstance(userDeliver).As<IContextDeliverer<ILanguageElement>>();

            builder.RegisterType<DatabaseReader>().As<IDatabaseReader<TranslatedPizza>>();
            return builder.Build();
        }
    }
}
