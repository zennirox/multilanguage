﻿using Autofac;
using DatabaseService;
using Infrastructure;
using MutliLang;

namespace MultiLang
{
    class Program
    {
        static void Main(string[] args)
        {
            var containerConfig = new ContainerConfigurator();

            var container = containerConfig.Configure(
                new ContainerBuilder(),
                containerConfig.BuildDelivers(Language.Polish)
                );

            using (container.BeginLifetimeScope())
            {
                var db = container.Resolve<IDatabaseReader<TranslatedPizza>>();
                db.ConnectDatabase("localhost:27017");
                var res = db.GetData();
            }
        }
    }
}
