﻿using System.Collections.Generic;

namespace Infrastructure
{
    public interface IDatabaseReader<T>
    {
        /// <summary>
        /// Read data from database
        /// </summary>
        /// <exception cref="DatabaseReaderException">Cannot get database</exception>
        /// <exception cref="DatabaseReaderException">There is no column : pizza in database</exception>
        /// <returns></returns>
        ICollection<T> GetData(string dbName, string columnName);

        /// <summary>
        /// Connect to passed db address
        /// </summary>
        /// <param name="dbAddress"></param>
        void ConnectDatabase(string dbAddress);
    }
}
