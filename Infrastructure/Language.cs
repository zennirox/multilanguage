﻿using System;

namespace Infrastructure
{
    public static class Language
    {
        public static string Polish = "pl-PL";
        public static string English = "en-GB";
        public static string Any = "Any";
    }
}
