﻿using System.Collections.Generic;

namespace Infrastructure
{
    public interface IContextDeliverer<TContext> where TContext : ILanguageElement
    {
        TContext DeliverData(ICollection<TContext> context);
    }

    public interface IUserDeliver
    {
        void SetSupportedLanguage(string supportedLanguage);
    }
}
