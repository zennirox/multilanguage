﻿namespace Infrastructure
{
    public interface ILanguageElement
    {
        string LanguageCode { get; }
    }
}
