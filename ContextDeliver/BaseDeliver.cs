﻿using System;
using System.Collections.Generic;
using System.Linq;
using Infrastructure;

namespace ContextDeliver
{
    public abstract class BaseDeliver : IContextDeliverer<ILanguageElement> 
    {
        protected BaseDeliver Next { get; private set; }
        protected string SupportedLanguage { get; set; }
        protected BaseDeliver(string language)
        {
            SupportedLanguage = language;
        }
        public BaseDeliver SetNext(BaseDeliver nextTranslator)
        {
            Next = nextTranslator;
            return nextTranslator;
        }
        public ILanguageElement DeliverData(ICollection<ILanguageElement> context)
        {
            if (context.Any(c => c.LanguageCode == SupportedLanguage) || 
                SupportedLanguage == Language.Any)
            {
                return GetContext(context);
            }
            if (Next == null)
            {
                throw new InvalidOperationException("Next language not supported");
            }
            return Next.DeliverData(context);
        }
        protected abstract ILanguageElement GetContext(ICollection<ILanguageElement> pizzas);
        
    }
}
