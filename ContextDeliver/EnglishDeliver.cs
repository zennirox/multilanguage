﻿using System.Collections.Generic;
using System.Linq;
using Infrastructure;

namespace ContextDeliver
{
    public class EnglishDeliver : BaseDeliver
    {
        public EnglishDeliver(string language) : base(language)
        {
        }


        protected override ILanguageElement GetContext(ICollection<ILanguageElement> content)
        {
            return content.First(c => c.LanguageCode == SupportedLanguage.ToString());
        }
    }
}
