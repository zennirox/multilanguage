﻿using System.Collections.Generic;
using System.Linq;
using Infrastructure;

namespace ContextDeliver
{
    public class UserDeliver : BaseDeliver, IUserDeliver
    {
        public UserDeliver(string language) : base(language)
        {
        }

        public void SetSupportedLanguage(string supportedLanguage)
        {
            SupportedLanguage = supportedLanguage;
        }

        protected override ILanguageElement GetContext(ICollection<ILanguageElement> content)
        {
            return content.First(c => c.LanguageCode == SupportedLanguage.ToString());
        }
    }
}
