﻿using System.Collections.Generic;
using System.Linq;
using Infrastructure;

namespace ContextDeliver
{
    public class AnyDeliver : BaseDeliver
    {
        public AnyDeliver(string language) : base(language)
        {
        }


        protected override ILanguageElement GetContext(ICollection<ILanguageElement> content)
        {
            return content.FirstOrDefault();
        }
    }
}
